# Custom Checkboxes and Radio Buttons #

Turn your boring checkboxes and radio buttons into awesome ones!

## How it Works ##
This plugin turns
```
#!html
<input type="checkbox" name="checkbox[]" value="example" />
```
into
```
#!html
<div class="checkbox" data-value="checkbox 5" data-field="checkbox[]">
	<span></span>
	checkbox 5
</div>
<input type="hidden" name="checkbox[]" value="">
```
You can style the span how you want it!


## How to Use ##
### Step 1 - Load the Javascript and CSS ###

Look inside the js folder to find customcheckbox.min.js and load both of these files. Load jQuery first.
```
#!html
<script src="js/jquery.min.js"></script>
<script src="js/customcheckbox.min.js"></script>
```
Look inside the css folder to find customcheckbox.css and load it.
```
#!html
<link href="css/customcheckbox.css" rel="stylesheet" type="text/css" />	
```
### Step 2 - Call Functions ###

```
#!html

//Style the Radio Button
customCheckboxes();
//Style the Radio Button
customRadios();
```

### Step 3 - Enjoy! ###