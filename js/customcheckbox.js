/*
 * CustomCheckboxes v1.1.0
 * by Margaret Velez 
 */

function customCheckboxes(){
	var newBoxes = '';
	var i = 0;
	var c = 0;
	$('input[type=checkbox]').each(function(){
		var fieldName = $(this).attr('name');
		var fieldValue = $(this).attr('value');
		var fieldChecked = $(this).attr('checked');
		var fieldLabel = $(this).attr('data-label');
		if(fieldChecked == 'checked'){
			var extra = fieldValue;
			var active = 'yes';
		}else{
			var extra = '';
			var active = '';
		}
		if((fieldLabel == undefined) || (fieldLabel == '')){
			var fieldLabel = fieldValue;
		}else{
			var fieldLabel = fieldLabel;
		}
		$(this).replaceWith('<div class="checkbox '+active+'" data-value="'+fieldValue+'" data-field="'+fieldName+'"><span></span>'+fieldLabel+'</div><input type="hidden" name="'+fieldName+'" value="'+extra+'" />');
	});	
	//Now that they are look right. Let's give them some functionality
	activateCheckbox();
}
function customRadios(){
	var newBoxes = '';
	var i = 0;
	var fieldName = '';
	var extra = '';
	var active = '';
	$('input[type=radio]').each(function(){

		var newfieldName = $(this).attr('name');
		var fieldValue = $(this).attr('value');
		var fieldChecked = $(this).attr('checked');
		var fieldLabel = $(this).attr('data-label');
		
		if(fieldChecked == 'checked'){
			var extra = fieldValue;
			var active = 'yes';
			console.log(fieldValue);
		}
		if((fieldLabel == undefined) || (fieldLabel == '')){
			var fieldLabel = fieldValue;
		}else{
			var fieldLabel = fieldLabel;
		}
		if(fieldName != newfieldName){
			fieldName = newfieldName;
			$(this).replaceWith('<input class="radiof_'+newfieldName+'" type="hidden" name="'+newfieldName+'" value="'+extra+'" /><div class="radio" data-value="'+fieldValue+'" data-field="'+fieldName+'"><span></span>'+fieldValue+'</div>');
		}else{
			$(this).replaceWith('<div class="radio '+active+'" data-value="'+fieldValue+'" data-field="'+fieldName+'"><span></span>'+fieldLabel+'</div>');
		}
		i++;
	});	
	//Now that they are look right. Let's give them some functionality
	activateRadio();
}

function activateCheckbox(){
	$('.checkbox').click(function(){
		if($(this).hasClass('yes') == true){
			$(this).removeClass('yes');
			$(this).addClass('no');
			$(this).next('input').attr('value', '');
		}else{
			$(this).removeClass('no');
			$(this).addClass('yes');
			var thisval = $(this).attr('data-value');
			$(this).next('input').attr('value', thisval);
		}
	});
}
function activateRadio(){
	$('.radio').click(function(){
		var thisval = $(this).attr('data-value');
		var thisfield = $(this).attr('data-field');
		$(this).addClass('yes');
		$('.radio').not(this).removeClass('yes');
		$('.radiof_' + thisfield).attr('value', thisval);

	});
}




